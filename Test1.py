from selenium import webdriver
import unittest

class GoogleSearchTestingProject(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome("C:\\Users\\Sefi\\PycharmProjects\\FirstSeleniumTest\\Drivers\\chromedriver.exe")
        cls.driver.implicitly_wait(5)
        cls.driver.maximize_window()

    def test_search_for_automationstepbystep(self):
        self.driver.get("https://www.google.com/")
        self.driver.find_element_by_name("q").send_keys("Automation step by step")
        self.driver.find_element_by_name("btnK").click()

    def test_search_for_sefi(self):
        self.driver.get("https://google.com")
        self.driver.find_element_by_name("q").send_keys("Sefi Teshuva")
        self.driver.find_element_by_name("btnK").click()

    @classmethod
    def tearDownClass(cls):

        print ("Test Completedd"

if __name__ == '__main__':
    unittest.main()





